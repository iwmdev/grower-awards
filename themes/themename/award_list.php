<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php');
?>
    <section class="mainContent award-list col-sm-12">
        <div class="row">
            <div class="page-title col-md-6">
                <?php
                $title = new Area('Page Title');
                $title->display($c);
                ?>
            </div>
            <div class="page-filter col-md-6">
                <?php
                $filter = new Area('Page Filter');
                $filter->display($c);
                ?>
            </div>
        </div>
        <?php
        $main = new Area('Main');
        $main->display($c);
        ?>
    </section>
<?php
$this->inc('elements/footer.php');
?>