<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php');
?>
    <div class="row">
        <section class="col-sm-6">
            <section class="mainContent">
                <?php
                $main = new Area('Main');
                $main->display($c);
                ?>
            </section>
        </section>
    </div>
    <div class="row">
        <section class="award-selector col-sm-12">
            <?php
            $awards = new Area('Award Levels');
            $awards->display($c);
            ?>
        </section>
    </div>
<?php
$this->inc('elements/footer.php');
?>