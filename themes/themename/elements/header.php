<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE?>" lang="<?php echo LANGUAGE?>">
<head>
	<?php Loader::element('header_required'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />

	<!-- Styles -->
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath(); ?>/stylesheets/styles.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

	<!-- Scripts -->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<?php
$bg = new GlobalArea('Background Slider');
$bg->disableControls();
$bg->display();
?>
	<header>
		<div class="container">
			<div class="row">
				<nav class="navbar">
                    <div class="navbar-inner">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <article class="navbar-brand">
                                <?php
                                $logo = new GlobalArea('Site Name');
                                $logo->disableControls();
                                $logo->display();
                                ?>
                            </article>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="navbar-collapse">
                            <?php
                            $headerNav = new GlobalArea('Header Nav');
                            $headerNav->disableControls();
                            $headerNav->display();
                            ?>
                        </div>
                    </div>
				</nav>
			</div>
		</div>
	</header>
	<section class="trunk">
		<div class="container">