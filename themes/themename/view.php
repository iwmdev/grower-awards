<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
            <section class="mainContent col-sm-12">
                <?php
                    $a = new Area('Main');
                    $a->display($c);
                    print $innerContent;
                    $a = new Area('Main Extra');
                    $a->display($c);
                ?>
            </section>
<?php
    $this->inc('elements/footer.php');
?>