<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php');
?>
    <section class="mainContent award-page col-sm-12">
        <?php
        $main = new Area('Main');
        $main->display($c);
        ?>
        <div class="row">
            <article class="description col-md-6 col-md-push-6">
                <?php
                $description = new Area('Award Description');
                $description->display($c);
                ?>
            </article>
            <section class="feature col-md-6 col-md-pull-6">
                <?php
                $description = new Area('Award Feature');
                $description->display($c);
                ?>
            </section>
        </div>
        <?php
        $main = new Area('Main Extra');
        $main->display($c);
        ?>
    </section>
<?php
$this->inc('elements/footer.php');
?>