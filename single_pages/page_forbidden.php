<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="row">
	<article class="error404 col-sm-12">
		<span class="headerBorder clearfix text-center"><h1 class="error"><?php echo t('Hold On There! (Error 403)')?></h1></span>
	</article>
</div>

<div class="row">
	<section class="search col-sm-8 col-sm-offset-2">
		<p><?php echo t('Our top secret Antenna Lab hides behind this wall and you do not have the clearance level to access it.')?></p>
		<p><?php echo t('If you believe this is a mistake, please let us know by submitting an issue through our <a href="'.$this->url('/contact').'">Contact Us</a> page.')?></p>
		<p><?php echo t('Or try and use the search below to find what you were looking for.')?></p>
		<?php  if (is_object($c)) { ?>
			<?php  $a = new Area("Search"); $a->display($c); ?>
		<?php  } ?>
	</section>
</div>