<?php         

defined('C5_EXECUTE') or die(_("Access Denied."));

class DojoSupersizedPackage extends Package {

	protected $pkgHandle = 'dojo_supersized';
	protected $appVersionRequired = '5.4.0';
	protected $pkgVersion = '1.2.2'; 
	
	public function getPackageName() {
		return t("Dojo Supersized"); 
	}	
	
	public function getPackageDescription() {
		return t("Scalable, fullscreen background image gallery with thumbnail navigation option");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('dojo_supersized', $pkg);
	}
	
}