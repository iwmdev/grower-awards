<?php      defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<div style="padding: 5px;">
	<label for="fsID"><?php           echo t('Select your fileset (Min image size = 1024x768px):');?></label>
    <br />
	<select id="fsID" name="fsID">
		<option value="0">--Choose Fileset--</option>

		<?php      foreach ($fileSets as $fs): ?>
		<option value="<?php      echo $fs->fsID; ?>"<?php      echo ($fsID == $fs->fsID ? ' selected="selected"' : ''); ?>>
			<?php      echo htmlspecialchars($fs->fsName, ENT_QUOTES, APP_CHARSET); ?>
		</option>
		<?php      endforeach ?>

	</select>
     <br /> <br />
    <label for="dsrandom"><?php           echo t('Randomise images?');?></label>
     <br />
        <select name="dsrandom" class="ccm-input-select ccm-file-set-id">
                <option value="1"    <?php          echo ($dsrandom == "1"?'selected':'')?>      >Yes</option>
                <option value="0"   <?php          echo ($dsrandom == "0"?'selected':'')?>     >No</option>
        </select>
         <br /> <br />
        <label for="dsinterval"><?php           echo t('Length between transitions (5000 is normal)');?></label>
         <br />
        <input type="text" id="dsinterval" name="dsinterval" size="4" value="<?php         echo $dsinterval?>"/>
         <br /> <br />
        <label for="dstransition"><?php           echo t('Transition type');?></label>
         <br />
        <select name="dstransition" class="ccm-input-select ccm-file-set-id">
                <option value="6"    <?php          echo ($dstransition == "6"?'selected':'')?>    >Carousel Right</option>
                <option value="7"   <?php          echo ($dstransition == "7"?'selected':'')?>     >Carousel Left</option>
                <option value="5"   <?php          echo ($dstransition == "5"?'selected':'')?>     >Slide Left</option>
                <option value="3"   <?php          echo ($dstransition == "3"?'selected':'')?>     >Slide Right</option>
                <option value="4"   <?php          echo ($dstransition == "4"?'selected':'')?>     >Slide Bottom</option>
                <option value="2"   <?php          echo ($dstransition == "2"?'selected':'')?>     >Slide Top</option>
                <option value="1"   <?php          echo ($dstransition == "1"?'selected':'')?>     >Fade</option>
                <option value="0"   <?php          echo ($dstransition == "0"?'selected':'')?>     >None</option>
        </select>
         <br /> <br />
         <label for="dstransitionspeed"><?php           echo t('Speed of transition (1000 is normal)');?></label>
          <br />
        <input type="text" id="dstransitionspeed" name="dstransitionspeed" size="4" value="<?php         echo $dstransitionspeed?>"/>
         <br /> <br />

<label for="dsvertical"><?php           echo t('Center vertical?');?></label>
 <br />
        <select name="dsvertical" class="ccm-input-select ccm-file-set-id">
                <option value="1"    <?php          echo ($dsvertical == "1"?'selected':'')?>      >Yes</option>
                <option value="0"   <?php          echo ($dsvertical == "0"?'selected':'')?>     >No</option>
        </select>
         <br /> <br />
        
        <label for="dshorizontal"><?php           echo t('Center horizontal?');?></label>
         <br />
        <select name="dshorizontal" class="ccm-input-select ccm-file-set-id">
                <option value="1"    <?php          echo ($dshorizontal == "1"?'selected':'')?>      >Yes</option>
                <option value="0"   <?php          echo ($dshorizontal == "0"?'selected':'')?>     >No</option>
        </select>
         <br /> <br />

<label for="dskeyboard"><?php           echo t('Allow keybooard controls?');?></label>
 <br />
        <select name="dskeyboard" class="ccm-input-select ccm-file-set-id">
                <option value="1"    <?php          echo ($dskeyboard == "1"?'selected':'')?>      >Yes</option>
                <option value="0"   <?php          echo ($dskeyboard == "0"?'selected':'')?>     >No</option>
        </select>
        
</div>

<hr />
