 <?php     
 // THIS IS THE UPDATE TEMPLATE WITH NAV //
 
 defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
 
 
 
 <script type="text/javascript">  
			
			jQuery(function($){
				$.supersized({
					
					autoplay			:1,
					fit_always			:0,
					fit_landscape		:1,
					fit_portrait		:1,
					horizontal_center	:<?php  echo $dshorizontal ; ?>,
					image_protect		:1,
					keyboard_nav		:<?php  echo $dskeyboard ; ?>,
					min_height			:768,
					min_width			:1024,
					new_window			:0,
					pause_hover			:0,
					performance			:0,
					random				: <?php  echo $dsrandom; ?>,
					
					slideshow			:1,
					slide_interval		: <?php  echo $dsinterval; ?>,
					slide_links			:1,
					start_slide			:1,
					stop_loop			:0,
					thumb_links			:0,
					thumbnail_navigation	:1,
					transition			: <?php  echo $dstransition; ?>,
					transition_speed	: <?php  echo $dstransitionspeed ; ?>,
					vertical_center		: <?php  echo $dsvertical ; ?>,


					//Images
					slides 					:  	[		
					
<?php    
$js = array();
foreach ($images as $img) {
  $js[] = "{image : '" . $img['fullSrc'] . "', title : '" . htmlspecialchars($img['title'], ENT_QUOTES, APP_CHARSET) . "'}";
}
echo implode(',', $js);
?>
		
												]
												
				}); 
		    });
		    
	</script>
    
    
    <!--Thumbnail Navigation-->
	<div id="prevthumb"></div> <div id="nextthumb"></div>
	
	<!--Control Bar-->
	<div id="controls-wrapper">
		<div id="controls">
		
			<!--Slide counter-->
			<div id="slidecounter">
				<span class="slidenumber"></span>/<span class="totalslides"></span>
			</div>
			
			<!--Slide captions displayed here-->
			<div id="slidecaption"></div>
			
			
		</div>
	</div>
	