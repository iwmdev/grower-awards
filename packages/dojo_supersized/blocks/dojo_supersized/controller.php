<?php    
	defined('C5_EXECUTE') or die(_("Access Denied."));

	class DojoSupersizedBlockController extends BlockController {
		
		var $pobj;
		
		protected $btName = 'Dojo Supersized';
		protected $btDescription = 'Scalable, fullscreen background image gallery with thumbnail navigation option';
		protected $btTable = 'btDojoSupersized';
		protected $btInterfaceWidth = "300";
		protected $btInterfaceHeight = "350";
		
		public $fullWidth = 1024;
		public $fullHeight = 768;
		public $thumbWidth = 0;
		public $thumbHeight = 0;
		public $cropThumbs = false;
		
		public function on_page_view() {
			$html = Loader::helper('html');				
			$bv = new BlockView();
			$bv->setBlockObject($this->getBlockObject());
			
			
		}
		// Specific for Gallery
	public $dsrandom_default	= 1;
	public $dsinterval_default = 5000;	
	public $dstransition_default = 6;
	public $dstransitionspeed_default = 1000;
	public $dskeyboard_default = 1;
	public $dshorizontal_default = 1;
	public $dsvertical_default = 1;
	
		public function save($data) {
		$data['DSrandom']= (is_numeric($data['DSrandom']) && $data['DSrandom'] >= 0) ? $data['DSrandom'] :  $this->dsrandom_default; 
		$data['DSinterval']= (is_numeric($data['DSinterval']) && $data['DSinterval'] >= 0) ? $data['DSinterval'] :  $this->dsinterval_default;  
		$data['DStransition']= (is_numeric($data['DStransition']) && $data['DStransition'] >= 0) ? $data['DStransition'] :  $this->dstransition_default;   
		$data['DStransitionspeed']= (is_numeric($data['DStransitionspeed']) && $data['DStransitionspeed'] >= 0) ? $data['DStransitionspeed'] :  $this->dstransitionspeed_default;  
		$data['DSkeyboard']= (is_numeric($data['DSkeyboard']) && $data['DSkeyboard'] >= 0) ? $data['DSkeyboard'] :  $this->dskeyboard_default;  
		$data['DShorizontal']= (is_numeric($data['DShorizontal']) && $data['DShorizontal'] >= 0) ? $data['DShorizontal'] :  $this->dskeyboard_default;  
		$data['DSvertical']= (is_numeric($data['DSvertical']) && $data['DSvertical'] >= 0) ? $data['DSvertical'] :  $this->dskeyboard_default;  
		
		parent::save($data);
	}
		
		public function view() {
			$ih = Loader::helper('image');
			$files = $this->getPermittedFilesetImages($this->fsID);
			$images = array();
			foreach ($files as $f) {
				$image = array();
				$image['fID'] = $f->fID;
				$image['title'] = $f->getTitle();
				$image['description'] = $f->getDescription();
				
				if ($this->fullWidth > 0 && $this->fullHeight > 0) {
					$resizeWidth = empty($this->fullWidth) ? 9999 : $this->fullWidth;
					$resizeHeight = empty($this->fullHeight) ? 9999 : $this->fullHeight;
					$image['fullSrc'] = $f->getRelativePath();
					$size = @getimagesize($f->getPath());
					$image['fullWidth'] = $size[0];
					$image['fullHeight'] = $size[1];
				}

				if ($this->thumbWidth > 0 && $this->thumbHeight > 0) {
					$resizeWidth = empty($this->thumbWidth) ? 9999 : $this->thumbWidth;
					$resizeHeight = empty($this->thumbHeight) ? 9999 : $this->thumbHeight;
					$thumb = $ih->getThumbnail($f, $resizeWidth, $resizeHeight, $this->cropThumbs);
					$image['thumbSrc'] = $thumb->src;
					$image['thumbWidth'] = $thumb->width;
					$image['thumbHeight'] = $thumb->height;
				} else {
					$image['thumbSrc'] = '';
					$image['thumbWidth'] = 0;
					$image['thumbHeight'] = 0;
				}

				$images[] = $image;
			}
			
			$this->set('images', $images);
		}
		
		public static function getPermittedFilesetImages($fsID) {
			Loader::model('file_set');
			Loader::model('file_list');

			$fs = FileSet::getByID($fsID);
			$fl = new FileList();		
			$fl->filterBySet($fs);
			$fl->filterByType(FileType::T_IMAGE);
			$fl->sortByFileSetDisplayOrder();

			$all_files = $fl->get();
			$permitted_files = array();
			foreach ($all_files as $f) {
				$fp = new Permissions($f);
				if ($fp->canRead()) {
					$permitted_files[] = $f;
				}
			}
			return $permitted_files;	
		}
		
		public function add() {
			$this->set('fsID', 0);
			$this->loadFileSets();
		}
		
		public function edit() {
			$this->loadFileSets();
		}
		
		private function loadFileSets() {
			Loader::model('file_set');
			$fileSets = FileSet::getMySets();
			$this->set('fileSets', $fileSets);
		}
		
	}

?>
