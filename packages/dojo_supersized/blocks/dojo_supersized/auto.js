function ccmValidateBlockForm() {
	if ($('select#fsID').val() == '0') {
		ccm_addError('You must choose a file set.');
	}
	if ($('input#dstransitionspeed').val() == '') {
		ccm_addError('You must enter a transition speed.');
	}
	if ($('input#dsinterval').val() == '') {
		ccm_addError('You must enter an interval speed.');
	}
	 
	return false;
}
