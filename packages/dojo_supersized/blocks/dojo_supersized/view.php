 <?php      defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
 <script type="text/javascript">  
			
			jQuery(function($){
				$.supersized({
				
					//Functionality
					slideshow               :   1,		//Slideshow on/off
					autoplay				:	1,		//Slideshow starts playing automatically
					start_slide             :   1,		//Start slide (0 is random)
					random					: 	<?php        echo $dsrandom ; ?>,		//Randomize slide order (Ignores start slide)
					slide_interval          :   <?php        echo $dsinterval ; ?>,	//Length between transitions
					transition              :   <?php        echo $dstransition ; ?>, 		//0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	<?php        echo $dstransitionspeed ; ?>,	//Speed of transition
					new_window				:	0,		//Image links open in new window/tab
					keyboard_nav            :   <?php        echo $dskeyboard ; ?>,		//Keyboard navigation on/off
					performance				:	2,		//0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
					image_protect			:	1,		//Disables image dragging and right click with Javascript
					image_path				:	'img/', //Default image path

					//Size & Position
					min_width		        :   1024,		//Min width allowed (in pixels)
					min_height		        :   768,		//Min height allowed (in pixels)
					vertical_center         :   <?php        echo $dsvertical ; ?>,		//Vertically center background
					horizontal_center       :   <?php        echo $dshorizontal ; ?>,		//Horizontally center background
					fit_portrait         	:   1,		//Portrait images will not exceed browser height
					fit_landscape			:   0,		//Landscape images will not exceed browser width

					//Images
					slides 					:  	[		
					
<?php     
$js = array();
foreach ($images as $img) {
  $js[] = "{image : '" . $img['fullSrc'] . "', title : '" . htmlspecialchars($img['title'], ENT_QUOTES, APP_CHARSET) . "'}";
}
echo implode(',', $js);
?>
		
												]
												
				}); 
		    });
		    
	</script>
	